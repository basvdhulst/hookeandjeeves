# set minimum required cmake version
cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)

# set name of the project
project(hookeandjeeves)

# set user variables
set(INSTALL_DIR ${CMAKE_SOURCE_DIR}/bin)

# set warning flags when compiling with GNU or Clang
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR 
	"${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	set(warnings "-Wall Wextra -Werror")
endif()

# include header directories
# include_directories(${CMAKE_SOURCE_DIR}/inc)

# run CMakeLists.txt in subdirectories
# add_subdirectory(src)

# add all .cpp files in /src to SOURCES
file(GLOB SOURCES "src/*.cpp")

# add sources to executable
add_executable(${PROJECT_NAME} ${SOURCES})

# set installation rules
install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${INSTALL_DIR})
