
#include <cmath>
#include <string>
#include <iostream>

using namespace std;

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

// variables to be altered!
const int nVars = 2;
const string nNames[nVars] = {"R","t"};
double base[nVars] = {0.147,0.0011};
double stepSize[nVars] = {0.001,0.0001};
const double eps[nVars] = {0.0001,0.00001};

const double PI = 3.141592654;
const double R0 = base[0];			// initial guess of R
const double t0 = base[1];			// initial guess of t
const double r = 0.000008;
const double P = 250000;
const double E = 72E9;
const double L = 5;

double evalFunc(double tempBase[])
{
	// function to be altered!
	double R = tempBase[0];
	double t = tempBase[1];
	double Pe = (PI*PI*PI*E)/(L*L)*R*R*R*t;
	double Pl = 2*PI*0.605*E*t*t;
	double P0 = 375E6*2*PI*R*t;

	return 2*PI*R*t + r/((Pe-P)/P) + r/((Pl-P)/P) + r/((P0-P)/P);
}

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////


bool explorationMove(int varN, double tempBase[], double step[])
{
	double value = evalFunc(tempBase);
	tempBase[varN] += step[varN];

	if(evalFunc(tempBase) < value)
		return true;
	else {
		tempBase[varN] -= 2*step[varN];
		if(evalFunc(tempBase) < value)
			return true;
		else {
			tempBase[varN] += step[varN];
			return false;
		}
	}
}

bool totalExplorationMove(const int n, double tempBase[], double step[])
{
	bool boolp = false;

	for(int i = 0; i < n; i++) {
		if(explorationMove(i, tempBase, step) == true)
			boolp = true;
	}

	return boolp;
}

void arrayCopy(double a[], double b[])
{
	for(int i = 0; i < nVars; i++)
		a[i] = b[i];
}

void arrayPrint(double a[])
{
	for(int i = 0; i < nVars; i++)
		cout << nNames[i] << " = " << a[i] << endl;
}

void arrayAdd(double newVec[], double a[], double b[])
{
	for(int i = 0; i < nVars; i++)
		newVec[i] = a[i] + b[i];
}

void arraySub(double newVec[], double a[], double b[])
{
	for(int i = 0; i < nVars; i++)
		newVec[i] = a[i] - b[i];
}



int main()
{
	bool updated = false;
	bool ended = false;
	double tempBase[nVars], newBase[nVars], newBaseStep[nVars];
	arrayCopy(tempBase, base); // initializing

	updated = totalExplorationMove(nVars, tempBase, stepSize); // alters the base vector to the new base point, 'true' if new
	arrayPrint(tempBase);

	while(ended == false) {

		if(updated == true) {
			arrayCopy(newBase, tempBase);
			arraySub(tempBase,newBase,base);
			arrayAdd(tempBase,newBase,tempBase);
			arrayCopy(newBaseStep,tempBase);
			arrayPrint(tempBase);

			updated = totalExplorationMove(nVars, tempBase, stepSize); // alters the base vector to the new base point, 'true' if new
			arrayCopy(base,newBase);

			if(updated == true)
				arrayCopy(newBase,tempBase);
			else {
				arrayCopy(tempBase,base);
				updated = totalExplorationMove(nVars, tempBase, stepSize); // alters the base vector to the new base point, 'true' if new
			}
		}
		else { // no update has taken place
			int endedCount = 0;
			for(int i = 0; i < nVars; i++) {
				if(stepSize[i] < eps[i]) // done optimizing
					endedCount++;
				else {// try smaller stepsize
					stepSize[i] /= 2;
					arrayCopy(tempBase,base);
					updated = totalExplorationMove(nVars, tempBase, stepSize); // alters the base vector to the new base point, 'true' if new
				}
			}

			if(endedCount == nVars)
				ended = true;
		}

		arrayPrint(tempBase);
	}

	// system("PAUSE");
}
