************************
Hooke and Jeeves (2010)
************************

Optimization algorithm by Hooke and Jeeves. Function and variables 
are hard-coded and need to be changed in the .cpp file.

NOTE: Algorithm seems not to work correctly, but since it is an old 
project I won't bother updating it.
